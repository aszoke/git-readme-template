# Git readme template

## Description

The Git readme template is a simple tool that allows developers to describe projects in a simple and efficient way.

## Features

* template for general Git readme

## Prerequisites

* OS requirement: -
* Framework requirement: -
* Knowledge requirement: [Markdown syntax](https://www.markdownguide.org/basic-syntax/)

# Installation 

Copy the [Git readme template](https://bitbucket.org/aszoke/git-readme-template/src/master/git-readme-template) to the targeted repository root and rename it to README.md.

### Verification

Not applicable.

## Usage

The template could be customized according to the needs.

__Example__:

* [DSA](https://bitbucket.org/aszoke/dsa/src/master/)

## Related projects

* [Awesome README](https://github.com/matiassingers/awesome-readme)

## Documentation (Where additional information could be found?)

* [Git README best practices](https://blog.kese.hu/2021/06/git-repository-readme-best-practices.html)

## Licence 

[![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, Akos Szoke has waived all copyright and related or neighboring rights to this work.